﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1.Model;
namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        IGameModel gm = new GameModel(2);
        [TestMethod]
        public void TestMethod1()
        {
            
            Assert.AreEqual(7, 7);
        }

        [TestMethod]
        public void placeAndRemoveTowersTest()
        {
            TowerType tt = new TowerType();
            gm.PlaceTower(new Pair(1, 2), tt);
            Assert.AreEqual(gm.CurrentGS, GameStatus.PLAYING);
            Assert.AreEqual(gm.Map.GetEntityList().Count, 1);
            gm.RemoveTower(new Pair(1, 2));
            Assert.AreEqual(gm.Map.GetEntityList().Count, 0);

        }

        [TestMethod]
        public void spawnEnemiesTest()
        {
            IWave w = gm.CurrentWave;
            Assert.IsTrue(w.HasEnemies());
            gm.SetReadyToSpawn(true);
            for (int i = 0; i <= GameConstants.MONSTER_SPAWN_RATE * 2; i++)
            {
                gm.Update();
            }
            for (int i = 0; i <= GameConstants.MONSTER_WALK_RATE * 2; i++)
            {
                gm.Update();
            }
            Assert.AreEqual(2L, gm.Map.GetEntityList().Count);
        }

        [TestMethod]
        public void goToNextWaveTest()
        {
            gm.NextWave();
            IWave w = gm.CurrentWave;
            w.ClearWave();
            gm.NextWave();
            Assert.AreEqual( 1, gm.Player.Wave);
        }

        [TestMethod]
        public void gameWinTest()
        {
            gm.Player.Wave = 22 ;
            gm.Update();
            Assert.AreEqual(gm.CurrentGS, GameStatus.WON);
        }

        [TestMethod]
        public void gameLostTest()
        {
            gm.SetReadyToSpawn(true);
            gm.Player.Hp = gm.Player.Hp - 999;
            gm.Update();
            Assert.AreEqual(gm.CurrentGS, GameStatus.LOST);
        }
    }
}

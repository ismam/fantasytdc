﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Model
{
    public interface IMap
    {

         void AddEntity(IEntity e);

        List<IEntity> GetEntityList();

        void RemoveEntity(Pair location);

        bool Positionable(Pair location);
    }
}

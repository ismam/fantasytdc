﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Model
{
    public interface IPlayer
    {
        int Coin { get; set; }
        int Hp { get; set; }
        int Wave { get; set; }
    }
        
}



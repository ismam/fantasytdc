﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Model
{
    public class Pair
    {
        public Pair(int i1, int i2)
        {
            this.Val1 = i1;
            this.Val2 = i2;
        }

        public int Val1 { get; }
        public int Val2 { get; }

    }
}

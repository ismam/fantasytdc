﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Model
{
    public class TowerType
    {
        int cost;

        public TowerType()
        {
            this.Cost = GameConstants.BASIC_TOWER_COST;
        }
        
        public int Cost 
        {
            get => cost;
            set => this.cost = value;
        }
    }
}

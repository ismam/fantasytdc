﻿namespace ConsoleApp1.Model
{
    public class Player : IPlayer
    {
        int coin;
        int hp;
        int wave;

        public Player()
        {
            this.coin = 1000;
            this.hp = 5;
            this.wave = 1;
        }
        int IPlayer.Coin { get => coin; set => coin = value; }
        int IPlayer.Hp { get => hp; set => hp = value; }
        int IPlayer.Wave { get => wave; set => wave = value; }
    }
}
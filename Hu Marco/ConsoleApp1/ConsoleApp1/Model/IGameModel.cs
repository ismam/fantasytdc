﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Model
{
    public interface IGameModel
    {
        bool PlaceTower(Pair location, TowerType tt);

        void RemoveTower(Pair location);

        GameStatus CurrentGS { get; }

        IPlayer Player { get; }

        IWave CurrentWave { get; }

        void NextWave();

        void Update();

        IMap Map { get; }

        void SetReadyToSpawn(bool b);

    }
}

﻿using System.Collections.Generic;

namespace ConsoleApp1.Model
{
    public class Wave : IWave
    {
        List<Enemy> el = new List<Enemy> { };
        int wave;
        public Wave(int n)
        {
            this.wave = n;
            for (int i = 0; i < 5; i++)
            {
                el.Add(new Enemy());
            }
        }

        public int CurrentWave
            {
            get => this.wave;
            set => this.wave = value; 
            }

        void IWave.ClearWave()
        {
            el.Clear();
        }

        bool IWave.HasEnemies()
        {
            return el.Capacity > 0;
        }

        IWave IWave.NextWave()
        {
            return new Wave(this.CurrentWave + 1);
        }

        IEnemy IWave.Spawn()
        {
            return new Enemy();
        }
    }
}
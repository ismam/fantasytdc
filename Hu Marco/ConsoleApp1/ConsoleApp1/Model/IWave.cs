﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Model
{
    public interface IWave
    {
        IWave NextWave();

        bool HasEnemies();

        IEnemy Spawn();
        void ClearWave();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Model
{
    public class GameModel : IGameModel
    {
        private static readonly int INITIAL_COINS = GameConstants.INITIAL_COINS;
        private static readonly int INITIAL_HP = GameConstants.INITIAL_HP;
        private static readonly int ENEMY_SPAWN_RATE = GameConstants.MONSTER_SPAWN_RATE;
        private static readonly int WAVES_TO_WIN = GameConstants.WAVES_TO_WIN;
        private  IMap m;
        private  IPlayer p;
        private IWave w;
        private GameStatus currentGS;
        private int tick;
        private bool readyToSpawn;

        public GameModel(int difficulty)
        {
            this.Map = new Map();
            this.Player = new Player();
            this.CurrentWave = new Wave(1);
            this.CurrentGS = GameStatus.PLAYING;
            this.readyToSpawn = false;
        }

        public bool PlaceTower(Pair location, TowerType tt)
        {
            if (p.Coin < tt.Cost && m.Positionable(location))
            {
                return false;
            }
            p.Coin = p.Coin-tt.Cost;
            ITower t = new BasicTower(location, tt);
            m.AddEntity(t);
            return true;
    }

        public void RemoveTower(Pair location)
        {
           this.m.RemoveEntity(location);
        }

        public GameStatus CurrentGS
        {
            get => currentGS;
            set => currentGS = value;
        }

        public IPlayer Player
        {
            get => p;
            set => this.p = value;
        }

        public IWave CurrentWave
        {
            get => w;
            set => this.w = value;
        }

        public void NextWave()
        {
            this.w = w.NextWave();
        }

        private void AddEntity(IEntity e)
        {
            m.AddEntity(e);
        }

        public void Update()
        {
            m.GetEntityList().ForEach(e=>e.Update());
            m.GetEntityList().ForEach(e =>
            {
                if (e.ShouldBeRemoved())
                {
                    m.RemoveEntity(e.Location);
                }
            });
            if (w.HasEnemies())
            {
                if (tick >= ENEMY_SPAWN_RATE && readyToSpawn)
                {
                    IEnemy e = w.Spawn();
                    this.AddEntity(e);
                    tick = 0;
                }
            }
            else
            {
                SetReadyToSpawn(false);
            }
            if (p.Hp <= 0)
            {
                this.CurrentGS = GameStatus.LOST;
                return;
            }
            if (p.Wave > WAVES_TO_WIN)
            {
                this.CurrentGS = GameStatus.WON;
                return;
            }
            this.tick++;
        }

        public IMap Map
        {
           get => m;
           set => this.m = value;
        }

        public void SetReadyToSpawn(bool b)
        {
            this.readyToSpawn = b;
        }

    }
}

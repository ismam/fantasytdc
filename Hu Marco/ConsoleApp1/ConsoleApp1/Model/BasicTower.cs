﻿namespace ConsoleApp1.Model
{
    internal class BasicTower : ITower
    {
        private Pair location;
        private TowerType tt;

        public BasicTower(Pair location, TowerType tt)
        {
            this.location = location;
            this.tt = tt;
        }

        public Pair Location => this.location;

        public bool ShouldBeRemoved()
        {
            return false;
        }

        public void Update()
        {
            ;
        }
    }
}
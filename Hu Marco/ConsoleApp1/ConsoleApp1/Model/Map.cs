﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleApp1.Model
{
    internal class Map : IMap
    {
        List<IEntity> entityList = new List<IEntity> { };

        public void AddEntity(IEntity e)
        {
            entityList.Add(e);
            
        }

        public List<IEntity> GetEntityList()
        {
            return this.entityList;
        }

        public bool Positionable(Pair location)
        {
            foreach (IEntity e in entityList)
            {
                if (e.Location.Val1 == location.Val1 &&
                    e.Location.Val2 == location.Val2)
                {
                    return true;
                }
            }
            return false;
        }

        public void RemoveEntity(Pair location)
        {
            List<IEntity> temp = new List<IEntity>(entityList);
            temp.ForEach(e =>
           {
               if (e.Location.Val1 == location.Val1 &&
                    e.Location.Val2 == location.Val2)
               {
                   entityList.Remove(e);
               }
           });
        }
    }
}
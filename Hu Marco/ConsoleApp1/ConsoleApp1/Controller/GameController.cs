﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1.Model;

namespace ConsoleApp1.Controller
{
    class GameController : IGameController
    {
        private Model.GameModel gm;
        private bool running;
        private int difficulty = 2;
        private GameLoop gl;

        public GameModel Model
        {
            get => this.gm;
        }

        public void HandleInput(IInput i)
        {
            gl.AddInput(i);
        }

        public void Init()
        {
            gm = new Model.GameModel(difficulty);
        }

        public void KillGameLoop()
        {
            throw new NotImplementedException();
        }

        public void PauseGame()
        {
            gl.Pause();
        }

        public void ResumeGame()
        {
            gl.Resume();
        }

        public int Difficulty
        {
            get => this.difficulty;
            set => this.difficulty = value;
        }

        public void StartLoop()
        {
            gl = new GameLoop(gm);
            if (!running)
            {
                this.running = true;
            }
        }

    }

}

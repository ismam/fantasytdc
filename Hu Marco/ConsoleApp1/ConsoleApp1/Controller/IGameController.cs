﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1.Model;

namespace ConsoleApp1.Controller
{
    interface IGameController
    {
        void Init();

        void PauseGame();

        void ResumeGame();

        void HandleInput(IInput i);

        void KillGameLoop();

        void StartLoop();
    }
}

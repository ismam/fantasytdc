﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1.Model;

namespace ConsoleApp1.Controller
{
    interface IInput
    {
        int GetInputType();
        int GetY();
        int GetX();
        TowerType GetTowerType();
    }
}

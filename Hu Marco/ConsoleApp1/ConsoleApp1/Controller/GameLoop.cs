﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ConsoleApp1.Model;
namespace ConsoleApp1.Controller
{
    class GameLoop
    {
        private static readonly int REFRESH_RATE = 500;
        private const int ADD_TOWER = 1;
        private const int REMOVE_TOWER = 2;
        private const int START_WAVE = 3;
        private readonly Model.GameModel gm;
        private bool running;
        private int i;

        private List<IInput> inputList;

        public GameLoop(Model.GameModel gm)
        {
            this.gm = gm;
  
            this.inputList = new List<IInput>();
            this.running = true;
        }

        public void Run()
        {
            if (running)
            {
                //input process
                this.ProcessInput();
                //model update
                gm.Update();
                //render view
            }
            else
            {
                Thread.Sleep(REFRESH_RATE);
            }
        }

        public void Resume()
        {
            this.running = true;
        }

        public void Pause()
        {
            this.running = false;
        }

        public void AddInput(IInput input)
        {
            this.inputList.Add(input);
        }

        private void ProcessInput()
        {
            if (inputList.Count() < 1)
            {
                inputList.ForEach(e=> {
                    switch (e.GetInputType())
                    {
                        case ADD_TOWER:
                            gm.PlaceTower(new Pair(e.GetX(), e.GetY()), e.GetTowerType());
                            break;
                        case REMOVE_TOWER:
                            gm.RemoveTower(new Pair(e.GetX(), e.GetY()));
                            break;
                        case START_WAVE:
                            gm.SetReadyToSpawn(true);
                            break;
                        default:
                            break;
                    }
                });
                inputList.Clear();
            }
        }
    }
}

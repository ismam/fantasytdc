﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TowerJojo;

namespace JojoTest
{
    [TestClass]
    public class UnitTest1
    {


        [TestMethod]
        public void TestMethod1()
        {
            EnemyType type = new EnemyType(100, 0, 25);
            IEnemy monster = new EnemyImpl(type);
            monster.Damage = 25;
            Assert.AreEqual(monster.Hp, 75);
        }


        [TestMethod]
        public void TestMethod2()
        {
            EnemyType type = new EnemyType(100, 0, 25);
            EnemyImpl monster = new EnemyImpl(type);

            HardMap mappa = new HardMap();
            monster.Path = mappa.getPathList();
            monster.spawn();
            monster.walk();

            Assert.AreEqual(monster.getLocation().Second, 10);
            Assert.AreEqual(monster.getLocation().First, 1);
            monster.walk();
            monster.walk();
            monster.walk();
            monster.walk();
            Assert.AreEqual(monster.getLocation().Second, 10);
            Assert.AreEqual(monster.getLocation().First, 5);

        }

    }
}

﻿namespace TowerJojo
{
    public interface IMapTile
    {
        /**
    * Method to get the position.
    * @return the position of the tile.
    */
        Pair<int, int> getPosition();

        /**
         * Method to set the position of the tile.
         * @param position position
         */
        void setPosition(Pair<int, int> position);

        /**
         * Method to get the status.
         * @return the status of the tile.
         */
        Status getStatus();

        /**
         * Method to set the status of the tile.
         * @param status status
         */
        void setStatus(Status status);
    }
    public enum Status
    {

        /**
         * If there isn't anything in the tile.
         */
        EMPTY,
        /**
         * If in the tile is a pathTile.
         */
        PATH,
        /**
         * If in the tile is a tower.
         */
        WITHTOWER,
        /**
         * If in the tile is an enemy.
         */
        WITHENEMY
    }
}
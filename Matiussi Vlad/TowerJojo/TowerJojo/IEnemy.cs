﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerJojo
{
    public interface IEnemy
    {
        /**
  * Method to implement enemies movement.
  */
        void walk();
        /**
         * Method to spawn an enemy.
         */
        void spawn();
        /**
         * Method to kill an enemy.
         */
        void death();
        /**
         * @return enemy health points.
         */
        int Hp { get; }
        /**
         * @return gold value dropped on enemy killed
         */
        int Value { get; }
        /**
        * @return speed of the enemy.
        */
        int Speed { get; }
        /**
         * Method to increment the speed of enemy
         * @param value value
         * @return
         */
        int SetSpeed { get; set; }
        /**
         * Method to set the damage inflict to an enemy.
         * @param damage damage.
         */
        int Damage { get; set; }
        /**
         * Method to set the map path.
         * @param path sentiero
         */
        List<IMapTile> Path { get; set; }
        /**
         * 
         * @return Direction.
         */
        Direction Direzione { get; }
        /**
         * Method to despawn an enemy.
         */
        void despawn();
        /**
         * 
         * @return enemy type
         */
        EnemyType EnemyType { get; }
    }

    public enum Direction
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    };
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerJojo
{
    class GameConstants
    {
        //*****GAME PARAMETERS*****//
        /** Milliseconds of each update. */
        public static int GAME_SPEED = 16;

        /** Waves reached for the win condition.*/
        public static int WAVES_TO_WIN = 20;

        //*****PLAYER PARAMETERS*****//
        /** Initial coins of the player.*/
        public static int INITIAL_COINS = 250;

        /** Initial hp of the player.*/
        public static int INITIAL_HP = 10;

        //*****ENEMY PARAMETERS*****//
        /** Ticks that a monster need to wait before spawning.*/
        public static int MONSTER_SPAWN_RATE = 25;

        /** Ticks before a monster can move forward.*/
        public static int MONSTER_WALK_RATE = 10;

        //SIMPLE
        /** Hp of the SIMPLE enemy.*/
        public static int SIMPLE_ENEMY_HP = 100;

        /** speed of the SIMPLE enemy.*/
        public static int SIMPLE_ENEMY_SPEED = 0;

        /** value of the SIMPLE enemy.*/
        public static int SIMPLE_ENEMY_VALUE = 25;

        //TANK
        /** Hp of the TANK enemy.*/
        public static int TANK_ENEMY_HP = 200;

        /** speed of the TANK enemy.*/
        public static int TANK_ENEMY_SPEED = 0;

        /** value of the TANK enemy.*/
        public static int TANK_ENEMY_VALUE = 30;

        //*****TOWER PARAMETERS*****//
        /** Ticks a basic tower need to wait before shooting the next projectile.*/
        public static int TOWER_SHOOT_RATE = 25;

        /** Ticks between the projectile being shot and enemy hit.*/
        public static int PROJECTILE_HIT_ENEMY = 1;

        //BASIC 1
        /** cost of the BASIC tower.*/
        public static int BASIC_TOWER_COST = 60;

        /** damage of the BASIC tower.*/
        public static int BASIC_TOWER_DAMAGE = 25;

        /** range of the BASIC tower.*/
        public static int BASIC_TOWER_RANGE = 1;

        /** shootspeed of the BASIC tower.*/
        public static int BASIC_TOWER_SPEED = 10;

        //RANGED
        /** cost of the RANGED tower.*/
        public static int RANGED_TOWER_COST = 75;

        /** damage of the RANGED tower.*/
        public static int RANGED_TOWER_DAMAGE = 25;

        /** range of the RANGED tower.*/
        public static int RANGED_TOWER_RANGE = 2;

        /** shootspeed of the RANGED tower.*/
        public static int RANGED_TOWER_SPEED = 25;

        //CANNON 3
        /** cost of the CANNON tower.*/
        public static int CANNON_TOWER_COST = 200;

        /** damage of the CANNON tower.*/
        public static int CANNON_TOWER_DAMAGE = 50;

        /** range of the CANNON tower.*/
        public static int CANNON_TOWER_RANGE = 3;

        /** shootspeed of the BASIC tower.*/
        public static int CANNON_TOWER_SPEED = 40;

        GameConstants() { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerJojo
{
    class WaveImpl : IWave
    {

        private static List<IMapTile> path = new List<IMapTile>();
        private List<IEnemy> ondata;
        private int numeroOndata;
        private EnemyType simple = new EnemyType(100,0,25);
        private EnemyType tank = new EnemyType(200, 0, 30);

        public WaveImpl(int numeroOndata)
        {
            ondata = new List<IEnemy>();
            this.numeroOndata = numeroOndata;
            if (this.numeroOndata > 15)
            {
                populate((numeroOndata) * 3, simple);
                populate((numeroOndata) * 2, simple);
            }
            populate((1 + numeroOndata) * 2, simple);
            if (this.numeroOndata > 10)
            {
                populate((numeroOndata) / 2, tank);
                populate((1 + numeroOndata) * 3, tank);

            }
        }

        public int Wave
        {
            get { return this.numeroOndata; }
        }

        public void clearWave()
        {
            this.ondata.Clear();
        }

        public bool hasEnemies()
        {
            return !ondata.Any();
        }

        public IWave nextWave()
        {
            return new WaveImpl(this.Wave + 1);
        }

        public void populate(int quantity, EnemyType type)
        {
            for (int a = 0; a < quantity; a++)
            {
                IEnemy e = new EnemyImpl(type);
                if (this.numeroOndata > 10)
                {
                    e.SetSpeed = e.Speed * 2;
                }
                e.Path = path;
                ondata.Add(e);
            }
        }

        public IEnemy spawn()
        {
            IEnemy e = null;
            if (!ondata.Any())
            {
                e = ondata[0];
                e.spawn();
                ondata.RemoveAt(0);
                return e;
            }
            throw new Exception();
        }
        public static List<IMapTile> Path 
        {
            get{ return path; }
            set{ path = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerJojo
{
    public class EnemyImpl : IEnemy
    {
        private static int TICKS_BEFORE_WALKING = GameConstants.MONSTER_WALK_RATE;
        private int hp;
        private int speed;
        private int value;
        private Boolean alive;
        private Direction direction;
        private List<IMapTile> path;
        private IMapTile actual;
        private int x = 1;
        private int tick;
        private EnemyType type;

        public EnemyImpl(EnemyType type)
        {
            this.type = type;
            this.hp = type.Health;
            this.speed = TICKS_BEFORE_WALKING + type.Speed;
            this.value = type.Value;
            this.alive = false;
        }

         public Pair<int, int> getLocation() {
        return actual.getPosition();
    }

        public void update()
        {
            this.death();
            if (tick >= speed)
            {
                this.walk();
                tick = 0;
            }
            else
            {
                tick++;
            }
        }

        public void walk()
        {
            if (path == null)
            {
                throw new Exception();
            }
            else
            {
                if (x > path.Count)
                {
                    this.despawn();
                }
                if (alive)
                {
                    IMapTile next = new MapTileImpl(path[x].getPosition().First,
                        path[x].getPosition().Second);
                    if (next.getPosition().First > actual.getPosition().First)
                    {
                        direction = Direction.RIGHT;
                    }
                    if (next.getPosition().First < actual.getPosition().First)
                    {
                        direction = Direction.LEFT;
                    }
                    if (next.getPosition().Second > actual.getPosition().Second)
                    {
                        direction = Direction.DOWN;
                    }
                    if (next.getPosition().Second < actual.getPosition().Second)
                    {
                        direction = Direction.UP;
                    }
                    actual = next;
                    x++;
                }
            }
        }

        public void death()
        {
            if (hp <= 0)
            {
                alive = false;
            }
        }

        public void despawn()
        {
            alive = false;
        }

        public Direction Direzione
        {
            get
            {
                return this.direction;
            }
        }

        public EnemyType EnemyType
        {
            get
            {
                return this.type;
            }
        }

        public int Hp
        {
            get
            {
                return this.hp;
            }
        }
        public int Speed
        {
            get
            {
                return this.speed;
            }
        }

        public int Value
        {
            get
            {
                return this.value;
            }
        }

        public int Damage
        {
            get { return this.hp; }
            set { this.hp -= value; }

        }

        public List<IMapTile> Path
        {
            get { return this.path; }
            set { this.path = value; }
        }

        public int SetSpeed
        {
            get { return this.speed; }
            set { this.speed = value; }
        }

        public void spawn()
        {
            alive = true;
        }
        public Boolean ShouldBeRemoved
        {
            get { return !this.Alive; }
        }
        public Boolean Alive
        {
            get{ return this.alive; }
        }
    }
}

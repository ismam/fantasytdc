﻿namespace TowerJojo
{
    public class EnemyType
    {

        public enum EnemyTypes
        {
            /**
             * Basic type of enemy.
             */
            SIMPLE,
            /**
             * Enemy with more hp but slower.
             */
            TANK

        }

        private int health, speed, value;
        public EnemyType(int health, int speed, int value)
        {
            this.health = health;
            this.speed = speed;
            this.value = value;
        }
        /**
 * 
 * @return hp of enemy.
 */
        public int Health
        {
            get{ return this.health; }
        }
        /**
         * 
         * @return speed value of enemy.
         */
        public int Speed
        {
            get { return this.speed; }
        }
        /**
         * 
         * @return value of gold
         */
        public int Value
        {
            get { return this.value; }
        }
    }

}
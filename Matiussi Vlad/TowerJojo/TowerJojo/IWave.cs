﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace TowerJojo
{
    interface IWave
    {
        /**
 * 
 * @return wave number.
 */
        int Wave { get; }
        /**
         * Method to insert enemies in a wave.
         * @param quantity quantity
         * @param type type
         */
        void populate(int quantity, EnemyType type);
        /**
         * Method to spawn enemies.
         * @return enemy
         */
        IEnemy spawn();
        /**
         * Method to get next wave.
         * @return wave
         */
        IWave nextWave();
        /**
         * 
         * @return true if wave has enemies.
         */
        Boolean hasEnemies();


        /**
         * clear the current wave's enemy.
         */
        void clearWave();
    }
}

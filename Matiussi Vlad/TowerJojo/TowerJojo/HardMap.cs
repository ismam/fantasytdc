﻿using System.Collections.Generic;
using System;

namespace TowerJojo
{
    public class HardMap
    {

        private List<IMapTile> enemyPath = new List<IMapTile>();
        private List<IMapTile> tiles = new List<IMapTile>();


        public HardMap()
        {
            generateGrid();
            generatePath();
        }


        public List<IMapTile> getPathList()
        {
            return this.enemyPath;
        }

        public List<IMapTile> getTileList()
        {
            return this.tiles;
        }

        private void generateGrid()
        {
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    IMapTile tile = new MapTileImpl(i, j);
                    tiles.Add(tile);
                }
            }
        }

        void generatePath()
        {
            for (int i = 0; i < 20 * 20; i++)
            {
                if (this.getTileList()[i].getPosition().Second == 10)
                {
                    this.getTileList()[i].setStatus(Status.PATH);
                    this.getPathList().Add(this.getTileList()[i]);
                }
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project1.observer
{
    public interface IObserver
    {
        void update(ObservableEntity subject);
    }
}

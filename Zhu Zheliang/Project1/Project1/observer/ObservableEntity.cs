﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Project1.Utils;
using Project1.observer;
using Project1.model;

namespace Project1.observer
{
    public abstract class ObservableEntity
    {
        private List<IObserver> observers = new List<IObserver>();

        public void AddObserver(IObserver o)
        {
            this.observers.Add(o);
        }

        public void NotifyObservers()
        {
            foreach(IObserver observer in observers)
            {
                observer.update(this);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Project1.Utils;

namespace Project1.model
{
    public class MapTile
    {
        private Pair<int, int> p;
        public MapTile(int x, int y)
        {
            p = new Pair<int, int>(x, y);
        }

        public Pair<int,int> Position
        {
            get { return p; }
        }
    }
}

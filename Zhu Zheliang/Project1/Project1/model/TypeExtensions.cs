﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Project1.Utils;

namespace Project1.model
{
    public static class TypeExtensions
    {
        public static int GetCost (this TowerType type)
        {
            switch (type)
            {
                case TowerType.CANNON:
                    return GameConstants.CANNON_TOWER_COST;
                case TowerType.RANGED:
                    return GameConstants.RANGED_TOWER_COST;
                default:
                    return GameConstants.BASIC_TOWER_COST;
            }
        }
        public static int GetDamage(this TowerType type)
        {
            switch (type)
            {
                case TowerType.CANNON:
                    return GameConstants.CANNON_TOWER_DAMAGE;
                case TowerType.RANGED:
                    return GameConstants.RANGED_TOWER_DAMAGE;
                default:
                    return GameConstants.BASIC_TOWER_DAMAGE;
            }
        }
        public static int GetRange(this TowerType type)
        {
            switch (type)
            {
                case TowerType.CANNON:
                    return GameConstants.CANNON_TOWER_RANGE;
                case TowerType.RANGED:
                    return GameConstants.RANGED_TOWER_RANGE;
                default:
                    return GameConstants.BASIC_TOWER_RANGE;
            }
        }
        public static int GetSpeed(this TowerType type)
        {
            switch (type)
            {
                case TowerType.CANNON:
                    return GameConstants.CANNON_TOWER_SPEED;
                case TowerType.RANGED:
                    return GameConstants.RANGED_TOWER_SPEED;
                default:
                    return GameConstants.BASIC_TOWER_SPEED;
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project1.model
{
    interface ITower : IEntity
    {
        float ShootTime { get; }

        Enemy Target { get; }

        TowerType Type { get; set; }

        List<IEntity> Enemies { get; set; }

        bool isTargetSet();

        Projectile Projectile { get; }


    }

   
}

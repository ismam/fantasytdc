﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Project1.Utils;

namespace Project1.model
{
    public interface IEntity
    {
        Pair<int,int> Location { get; }

        void Update();

        bool ShouldBeRemoved();
    }
}

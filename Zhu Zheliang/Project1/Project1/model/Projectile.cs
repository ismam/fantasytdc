﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Project1.Utils;
using Project1.model;

namespace Project1.model
{
    public class Projectile : IEntity
    {
        private static readonly int PROJECTILE_HIT_ENEMY = GameConstants.PROJECTILE_HIT_ENEMY;
        private readonly Pair<int, int> position;
        private readonly Enemy enemy;
        private readonly int damage;
        private int tick;
        private bool alive;

        public Projectile(Pair<int, int> position, Enemy enemy, int damage)
            
        {
            this.position = position;
            this.damage = damage;
            this.enemy = enemy;
            this.alive = true;
        }

        public Pair<int, int> Location { get => position; }

        public bool ShouldBeRemoved()
        {
            return !alive;
            
        }

        public void Update()
        {
            if (alive)
            {
                if(tick > PROJECTILE_HIT_ENEMY)
                {
                    enemy.setDamage(damage);
                    this.alive = false;
                }
                this.tick++;
            }
            
        }
    }
}

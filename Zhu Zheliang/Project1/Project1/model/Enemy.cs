﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Project1.model;
using Project1.observer;
using Project1.Utils;

namespace Project1.model
{
   public class Enemy : ObservableEntity
    {
        private int hp = 10;
        private bool alive;


        public Enemy()
            :base()
        {
            this.alive = false;
        }

        public void setDamage(int damage)
        {
            this.hp -= damage;
            if (this.hp <= 0)
            {
                this.death();
                NotifyObservers();
            }
        }

        public void spawn()
        {
            alive = true;
        }

        public void despawn()
        {
            alive = false;
            NotifyObservers();
        }

        public void death()
        {
            if(hp <= 0)
            {
                alive = false;
            }
        }

        public int getHP()
        {
            return hp;
        }

        public bool isAlive()
        {
            return alive;
        }
    }
}

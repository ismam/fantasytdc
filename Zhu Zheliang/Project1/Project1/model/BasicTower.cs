﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Project1.Utils;
using Project1.observer;

namespace Project1.model
{
    public class BasicTower : ObservableEntity, ITower
    {
        private static readonly int GRID_SIZE = 20;
        private readonly Pair<int, int> position;
        private readonly int damage;
        private readonly int range;
        private readonly int shootTime;
        private Enemy target;
        private TowerType type;
        private List<Pair<int, int>> shootingZone;
        private List<IEntity> enemies;
        private Projectile projectile;
        private int tick;

        public float ShootTime => shootTime;

        public Enemy Target => target;

        public TowerType Type { get => type; set => this.type = value; }
        public List<IEntity> Enemies { get => enemies; set => this.enemies = value; }

        public Projectile Projectile => projectile;

        public BasicTower(Pair<int, int> position, TowerType type)
            : base()
        {
            this.position = position;
            this.damage = type.GetDamage();
            this.range = type.GetRange();
            this.target = null;
            this.type = type;
            this.shootTime = GameConstants.TOWER_SHOOT_RATE + type.GetSpeed();
            this.shootingZone = new List<Pair<int, int>>();
            setRange();
            this.enemies = new List<IEntity>();
        }

        public List<Pair<int,int>> ShootingZone{get => shootingZone;}
        private void findTarget()
        {
            foreach(IEntity e in enemies)
            {
                for(int i = 0; i < shootingZone.Capacity; i++)
                {
                    if(e is Enemy)
                    {
                        this.target = (Enemy) e;
                        return;
                    }
                    else
                    {
                        throw new System.ArgumentException();

                    }
                }
            }
        }

        private void setRange()
        {
            for (int i = position.First - range; i <= position.First + range; i++)
            {
                for (int j = position.Second- range; j <= position.Second + range; j++)
                {
                    if (position.First < GRID_SIZE && position.Second < GRID_SIZE)
                    {
                        shootingZone.Add(new Pair<int,int>(i, j));
                    }
                }
            }
        }

        private Projectile shoot()
        {
            return new Projectile(position, target, damage);
        }

        public Pair<int, int> Location { get => position; }

        public bool isTargetSet()
        {
            if (target == null)
            {
                return false;
            }
            return true;
        }


        public bool ShouldBeRemoved()
        {
            return false;
        }


        public void Update()
        {
            findTarget();
            if(isTargetSet() && this.tick >= shootTime)
            {
                this.projectile = shoot();
                this.tick = 0;
                this.NotifyObservers();
            }
        }
    }
}

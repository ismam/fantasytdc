﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project1.Utils
{
    class GameConstants
    {
        public static readonly int SIMPLE_ENEMY_HP = 100;

        public static readonly int TANK_ENEMY_HP = 200;

        public static readonly int TOWER_SHOOT_RATE = 25;

        public static readonly int PROJECTILE_HIT_ENEMY = 1;

        public static readonly int BASIC_TOWER_COST = 60;

        public static readonly int BASIC_TOWER_DAMAGE = 25;

        public static readonly int BASIC_TOWER_RANGE = 1;

        public static readonly int BASIC_TOWER_SPEED = 10;

        public static readonly int RANGED_TOWER_COST = 75;

        public static readonly int RANGED_TOWER_DAMAGE = 25;

        public static readonly int RANGED_TOWER_RANGE = 2;

        public static readonly int RANGED_TOWER_SPEED = 25;

        public static readonly int CANNON_TOWER_COST = 200;

        public static readonly int CANNON_TOWER_DAMAGE = 50;

        public static readonly int CANNON_TOWER_RANGE = 3;
       
        public static readonly int CANNON_TOWER_SPEED = 40;

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project1.Utils
{
    public class Pair<X,Y>
    {
        private  readonly X x;
        private  readonly Y y;

        public Pair(X first, Y second)
        {
            this.x = first;
            this.y = second;
        }

        public X First { get { return x; } }

        public Y Second { get { return y; } }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;
            if (obj == null)
                return false;
            Pair<X, Y> other = obj as Pair<X, Y>;
            if (other == null)
                return false;
            return
                (((First == null) && (other.First == null))
                || ((First != null) && First.Equals(other.First)))
                &&
                (((Second == null) && (other.Second == null))
                || ((Second != null) && Second.Equals(other.Second)));
        }

        public override int GetHashCode()
        {
            int hashcode = 0;
            if (First != null)
                hashcode += First.GetHashCode();
            if (Second != null)
                hashcode += Second.GetHashCode();

            return hashcode;
        }

        public void Stampa()
        {
            Console.WriteLine(First + "" + this.Second);
        }
    }
}

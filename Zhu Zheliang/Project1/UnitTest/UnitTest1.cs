﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Project1.model;
using Project1.Utils;
using System.Linq;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        private BasicTower t = new BasicTower(new Pair<int, int>(1, 2), TowerType.BASIC);
        private MapTile m = new MapTile(1, 2);
        private static Enemy target = new Enemy();
        private Projectile p = new Projectile(new Pair<int, int>(2, 3), target, TowerType.BASIC.GetRange());
        private MapTile mp = new MapTile(2, 3);
        [TestMethod]
        public void TestLocation()
        {
            //check the tower position
            Assert.AreEqual(m.Position, t.Location);
            Pair<int, int> b = new Pair<int,int>(1, 3);
            
            //check the tower range
            for (int i = 0; i < t.ShootingZone.Count; i++)
            {
                Pair<int, int> s = t.ShootingZone.ElementAt(i);
                if (s.Equals(b))
                {
                    Console.WriteLine(s);
                }
            }
        }

        public void testShoot()
        {
            t.Update();
            Assert.AreEqual(p.Location, mp.Position);
        }
    }
}


Per la parte di C# tradotto le classi BasicTower, Projectile, Enemy, Maptile e interfacce come ITower, IEntity.
Per TowerType ho usato un enum ed esteso in una classe chiamata TypeExtensions.
Ho tradotto anche le classi per il pattern Observer e un test per testare le posizioni delle torri e proiettili.

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FantasyTD.Model.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FantasyTD.Entity;
using FantasyTD.UtilityClasses;
using FantasyTDC.Model.Map;
using FantasyTDC.Model.Player;

namespace FantasyTD.Model.Map.Tests
{
    [TestClass()]
    public class AbstractIMapTests
    {
        [TestMethod()]
        public void AbstractIMapTest()
        {
            AbstractIMap m = new HardMap();
            MapTile m1 = new MapTile(0, 10);
            m1.Status= Status.PATH;
            Assert.AreEqual(m.PathList[0].Position, m1.Position);
            
            MapTile m2 = new MapTile(0, 20);
            m2.Status = Status.EMPTY;
            Assert.AreNotEqual(m.PathList[0].Status,m2.Status);
            
            Pair<int, int> p1 = new Pair<int, int>(0, 0);
            Assert.IsTrue(m.Positionable(p1));
            
            Pair<int, int> p2 = new Pair<int, int>(0, 10);
            Assert.IsFalse(m.Positionable(p2));
            
            Assert.AreEqual(m.GetTileInt(0).Position.First, p2.First);
            Assert.AreEqual(m.GetTilePair(p2).Status, Status.PATH);
            
            MapTile m3 = new MapTile(0, 0);
            m3.Status = Status.WITHENEMY;
            m.SetTile(m3);
            Assert.AreEqual(m.TileList[0].Status, Status.WITHENEMY);
            
            Pair<int, int> p3 = new Pair<int, int>(0, 0);

            Assert.AreNotEqual(m.FromIntToPair(1).Second, m3.Position.Second);
            
            Assert.AreEqual(m.FromPairToInt(m3.Position), 0);

            Assert.AreNotEqual(m.FromIntToPair(1), p3);

            Assert.AreEqual(m.FromPairToInt(p3), 0);

            AbstractIMap nm = new NormalMap();
            MapTile mt = new MapTile(5, 0);
            m1.Status = Status.PATH;
            Assert.AreEqual(m.PathList[0].Position, m1.Position);

            AbstractIMap sm = new SimpleMap();
            MapTile mt2 = new MapTile(5, 0);
            m1.Status = Status.PATH;
            Assert.AreEqual(m.PathList[0].Position, m1.Position);

            IPlayer pl = new Player("SexyIsmy", 10, 350);
            pl.setWave(13);
            pl.takeDamage(2);
            pl.incrementCoins(50);
            Assert.AreEqual(pl.Name, "SexyIsmy");
            Assert.AreEqual(pl.Wave,13);
            Assert.AreEqual(pl.Hp, 8);
            Assert.AreEqual(pl.Coins, 400);
            

        }
    }
}
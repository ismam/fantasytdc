﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FantasyTD;
using FantasyTD.Entity;
using FantasyTD.Model.Map;
using FantasyTD.UtilityClasses;

namespace FantasyTDC
{
    class Program
    {
        static void Main(string[] args)
        {
            Pair<int, int> p1 = new Pair<int, int>(1, 5);
            Pair<int, int> p2 = new Pair<int, int>(2, 4);
            Pair<int, int> p3 = new Pair<int, int>(3, 3);
            Pair<int, int> p4 = new Pair<int, int>(5, 15);
            Pair<int, int> p5 = new Pair<int, int>(33, 51);

            Console.WriteLine(p1.First + " " + p1.Second);

            AbstractIMap m = new HardMap();

            /*for (int i = 0; i < 400; i++)
            {
                Console.WriteLine("x: " + m.TileList[i].Position.First + "      y: " + m.TileList[i].Position.Second);

            }*/

            Console.WriteLine(m.PathList[0].Position.First + " " + m.PathList[0].Position.Second);

            Console.WriteLine(m.PathList[5].Position.First);
            Console.WriteLine(m.PathList[19].Position.Second);

            Console.Read();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyTDC.Model.Player
{
    public class Player : IPlayer
    {
        private String name;
        private int hp;
        private int coins;
        private int wave;

        public Player(String name, int hp, int coins)
        {
            this.name = name;
            this.hp = hp;
            this.coins = coins;
            this.wave = 1;
        }

        public string Name
        {
            get { return this.name; }
        }

        public int Hp
        {
            get { return this.hp; }
        }
        public int Coins
        {
            get { return this.coins; }
        }

        public int Wave
        {
            get { return this.wave; }
        }

        public void incrementCoins(int coins)
        {
            this.coins = this.coins + coins;
        }

        public void setName(string text)
        {
            this.name = text;
        }

        public void setWave(int wave)
        {
            this.wave = wave;
        }

        public void takeDamage(int damage)
        {
            this.hp = this.hp - damage;
        }
    }
}

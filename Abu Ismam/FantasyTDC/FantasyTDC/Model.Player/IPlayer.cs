﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyTDC.Model.Player
{
    public interface IPlayer
    {

        String Name { get; }
        
        int Hp { get;  }
        
        void takeDamage(int damage);
        
        int Coins { get;  }
        
        void incrementCoins(int coins);
        
        int Wave { get; }
    
        void setWave(int wave);

        void setName(String text);
    }
}

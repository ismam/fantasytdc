﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using FantasyTD.Entity;
using FantasyTD.UtilityClasses;

namespace FantasyTD.Model.Map
{
    public abstract class AbstractIMap : IMap
    {
        private List<MapTile> grid = new List<MapTile>();
        private List<MapTile> enemyPath = new List<MapTile>();
        private List<IEntity> entity = new List<IEntity>();
        public static readonly int size = 20;

        public AbstractIMap()
        {
            GenerateGrid();
            GeneratePath();
        }

        void GenerateGrid()
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    MapTile tile = new MapTile(i, j);
                    grid.Add(tile);
                }
            }
        }

        public abstract void GeneratePath(); 

        public List<IEntity> EntityList
        {
            get { return this.entity; }
        }

        public List<MapTile> TileList
        {
            get { return this.grid; }
        }

        public List<MapTile> PathList
        {
            get { return this.enemyPath; }
        }

        public static int Size
        {
            get
            {
                return size;
            }
        }

        public void AddEntity(IEntity e)
        {
            this.entity.Add(e);
        }

        public void RemoveEntity(Pair<int, int> location)
        {
            for (int i=0; i< EntityList.Capacity; i++)
            {
                if (EntityList[i].GetLocation().Equals(location))
                {
                    EntityList[i].ShouldBeRemoved();
                }
            }
        }

        public void RemoveEntity(IEntity e)
        {
            entity.Remove(e);
        }

        public MapTile InitialPosition()
        {
            return (MapTile) enemyPath[0];
        }

        public MapTile FinalPosition()
        {
            return (MapTile) enemyPath[size];
        }

        public bool Positionable(Pair<int, int> position)
        {
            MapTile m = GetTilePair(position);
            return m.Status == Status.EMPTY;
        }

        public MapTile GetTileInt(int position)
        {
            return (MapTile) grid[position];
        }

        public MapTile GetTilePair(Pair<int, int> position)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    MapTile tmp = (MapTile) grid[i];
                    if (tmp.Position.Equals(position))
                    {
                        return (MapTile) grid[i];
                    }
                }
            }
            return null;
            
        }

        public void SetTile(MapTile tile)
        {
            int position = tile.Position.First * size + tile.Position.Second;
            grid.RemoveAt(position);
            grid.Insert(position, tile);
        }

        public Pair<int, int> FromIntToPair(int position)
        {
            int x = position / size;
            int y = position % size;
            return new Pair<int,int>(x, y);
        }

        public int FromPairToInt(Pair<int, int> position)
        {
            return position.First * size + position.Second;
        }
    }
        
}

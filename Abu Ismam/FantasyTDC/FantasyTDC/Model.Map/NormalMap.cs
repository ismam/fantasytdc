﻿using FantasyTD.Model.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyTDC.Model.Map
{
    public class NormalMap : AbstractIMap
    {
        public override void GeneratePath()
        {
            for (int i = 0; i < Size * Size; i++)
            {   
                // da 5,0 a 5,10 da sx a dx
                if (TileList[i].Position.First == 5 && TileList[i].Position.Second <= 10)
                {
                    TileList[i].Status = Status.PATH;
                    PathList.Add(TileList[i]);
                }
            }

            for (int i = 0; i < Size * Size; i++)
            {   
                // da 5,10 a 15,10 da sopra a sotto
                if (TileList[i].Position.First >= 5 && TileList[i].Position.First <= 15 && TileList[i].Position.Second == 10)
                {
                    TileList[i].Status = Status.PATH;
                    PathList.Add(TileList[i]);
                }
            }

            for (int i = 0; i < Size * Size; i++)
            {    
                // da 15,10 a 15,20 da sx a dx
                if (TileList[i].Position.First == 15 && TileList[i].Position.Second >= 10)
                {
                    TileList[i].Status = Status.PATH;
                    PathList.Add(TileList[i]);
                }
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using FantasyTD.Entity;
using FantasyTD.UtilityClasses;

namespace FantasyTD.Model.Map
{
    public interface IMap
    {
        List<IEntity> EntityList { get; }

        List<MapTile> TileList { get; }

        List<MapTile> PathList { get; }

        void AddEntity(IEntity e);

        void RemoveEntity(Pair<int,int> location);

        void RemoveEntity(IEntity e);

        MapTile InitialPosition();

        MapTile FinalPosition();

        bool Positionable(Pair<int, int> position);

        MapTile GetTileInt(int position);

        MapTile GetTilePair(Pair<int, int> position);

        void SetTile(MapTile tile);

        Pair<int, int> FromIntToPair(int position);

        int FromPairToInt(Pair<int, int> position);

    }
}

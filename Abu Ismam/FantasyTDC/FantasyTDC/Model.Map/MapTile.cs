﻿using FantasyTD.UtilityClasses;
using System;
using System.Collections.Generic;
using System.Text;

namespace FantasyTD.Model.Map
{
    public class MapTile
    {
        private Pair<int, int> position;
        private Status status;

        public MapTile(int x, int y)
        {
            position = new Pair<int, int>(x, y);
            status = Status.EMPTY;
        }

        public Pair<int, int> Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        public Status Status
        {
            get { return this.status; }
            set { this.status = value; }
        }
    }
}

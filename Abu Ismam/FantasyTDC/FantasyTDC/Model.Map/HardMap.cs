﻿using FantasyTD.Model.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyTD.Model.Map
{
    public class HardMap : AbstractIMap
    {
        public override void GeneratePath()
        {
            for (int i = 0; i < Size * Size; i++)
            {
                if (TileList[i].Position.Second == 10)
                {
                    TileList[i].Status=Status.PATH;
                    PathList.Add(TileList[i]);
                }
            }
        }
    }
}

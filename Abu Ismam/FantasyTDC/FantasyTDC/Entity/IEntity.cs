﻿using FantasyTD.UtilityClasses;
using System;
namespace FantasyTD.Entity
{
    public interface IEntity
    {
        Pair<int, int> GetLocation();

        void Update();

        bool ShouldBeRemoved();
    }
}

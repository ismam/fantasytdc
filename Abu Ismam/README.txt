Il mio intento è stato tradurre principalmente le classi del Model della mappa ed il model del Player.
Per poter tradurre fedelmente tali classi ho anche dovuto tradurre anche l'interfaccia relativa all'Entity e la classe Pair.
Ho cercato di seguire le linee guida principali per programmare in c# spiegate a lezione.
Principalmente si basa tutto su AbstractIMap che implementa IMap e viene estesa da altre 3 classi per il path.
Il test invece si occupa di verificare il corretto funzionamento delle classi richiamando quasi ogni metodo per le classi più importanti.